# POKEDEX APP

Aplicacion web realizada con React, Redux, Bootstrap, permite realizar busquedas de pokemones de primera generación, observar sus atributos y habilidades principales y agregarlos a tu equipo.

## DEMO


demo de la aplicación en linea disponible en: [https://pokedex-app-react-redux.web.app/](https://pokedex-app-react-redux.web.app/)


## Scripts Disponibles

### `npm start`

Enciende la aplicación en el puerto [http://localhost:3000](http://localhost:3000)

### `npm run build`

Construye la aplicación en la carpeta `build`.

