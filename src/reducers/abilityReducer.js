import { GET_ABILITY, ERROR_ABILITY, LOADING_ABILITY } from "../types/abilityTypes"

const initialState = {
    data: {},
    loading: false,
    error: null
}

const abilityReducer = (state = initialState, { type, payload }) => {
    switch (type) {

        case GET_ABILITY:
            return {
                ...state,
                loading: false,
                error: null,
                data: payload
            }
        case ERROR_ABILITY:
            return {
                ...state,
                loading: false,
                error: payload
            }
        case LOADING_ABILITY:
            return {
                ...state,
                loading: true
            }
        default:
            return state
    }
}
export default abilityReducer;