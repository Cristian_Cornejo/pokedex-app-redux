import { GET_POKEMON, ERROR_POKEMON, LOADING_POKEMON } from "../types/pokemonTypes"

const initialState = {
    pokemon: null,
    error: null,
    loading: false
}

const pokemonReducer = (state = initialState, { type, payload }) => {
    switch (type) {

        case GET_POKEMON:
            return {
                ...state,
                pokemon: payload,
                loading: false
            }
        case ERROR_POKEMON:
            return {
                ...state,
                error: payload,
                loading: false
            }
        case LOADING_POKEMON:
            return {
                ...state,
                loading: true
            }

        default:
            return state
    }
}

export default pokemonReducer;