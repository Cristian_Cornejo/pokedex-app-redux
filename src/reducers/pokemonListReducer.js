import { GET_ALL_LIST, ERROR_LIST, LOADING_LIST, INDEX_SELECTED, FILTER_LIST } from "../types/pokemonListTypes"

const initialState = {
    list: [],
    filtered_list: [],
    loading: false,
    error: null,
    indexSelected: null,
    search: ''
}

const mapStateToProps = (state = initialState, { type, payload }) => {
    switch (type) {

        case GET_ALL_LIST:
            return {
                ...state,
                loading: false,
                error: null,
                list: payload,
                filtered_list: payload
            }
        case ERROR_LIST:
            return {
                ...state,
                loading: false,
                error: payload
            }
        case LOADING_LIST:
            return {
                ...state,
                loading: true
            }
        case INDEX_SELECTED:
            return {
                ...state,
                indexSelected: payload
            }
        case FILTER_LIST:
            return {
                ...state,
                search: payload,
                filtered_list: state.list.filter(pokemon =>
                    pokemon.name.toLowerCase().includes(payload.toLowerCase())
                )
            }
        default:
            return state
    }
}

export default mapStateToProps;