import React from 'react'
import '../styles/header.css'
const Header = () => (<div className='header pt-3 pb-3'>
    <h1>Pokedex</h1>
</div>);

export default Header;