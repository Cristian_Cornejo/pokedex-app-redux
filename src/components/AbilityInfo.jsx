import React from 'react';
import { connect } from 'react-redux';
import Layout from './shared/Layout';

const AbilityInfo = ({ data, error, loading, closeInfo }) => {
    return (
        <div className=' col-sm-12 dropdown-content'>
            <div className='mainInfo '>
                <div className=" pt-3">
                    <Layout loading={loading} error={error} >
                        <div className="container">
                            <div className='row '>
                                <div className='d-flex justify-content-between '>
                                    {`${data}`}
                                    <button type='button' onClick={closeInfo} className="btn-custom btn badge badge-pill">x</button>
                                </div>
                            </div>
                        </div>
                    </Layout>
                </div>
            </div>
        </div>
    );
}


const mapStateToProps = ({ abilityReducer }) => {
    return abilityReducer;
}

export default connect(mapStateToProps)(AbilityInfo);
