
import React from 'react';
import Layout from './Layout';

const Card = ({ children, loading, error }) => {

    return (
        <div className='mainInfo pb-2'>
            <div className="card  card-custom pt-3">
                <Layout loading={loading} error={error} >
                    <div className="container">
                        <div className='row'>
                            {children}
                        </div>
                    </div>
                </Layout>
            </div>
        </div>);
}

export default Card;