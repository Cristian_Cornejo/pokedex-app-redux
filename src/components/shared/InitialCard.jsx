import React from 'react';
import '../../styles/initialCard.css';
const InitialCard = ({ message }) => {
    return (<div className='initial-card align-middle text-center'>
        <h1 className='align-middle'>{message}</h1>
    </div>);
}

export default InitialCard;