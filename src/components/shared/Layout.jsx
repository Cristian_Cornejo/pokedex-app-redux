import React from 'react';
import LoadingSpinner from './LoadingSpinner';

const Layout = ({ children, loading, error }) => {
    if (error) return (<div>error</div>);
    if (loading) return (<LoadingSpinner />);
    return (<React.Fragment>
        {children}
    </React.Fragment>)
}

export default Layout;
