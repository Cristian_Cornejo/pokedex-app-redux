import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import Card from './shared/Card';
import InitialCard from './shared/InitialCard';
import PokeImage from './shared/PokeImage';
import * as teamActions from '../actions/teamActions';

const PokemonMainInfo = ({ pokemon, error, loading, addToTeam, team }) => {

    const catchPokemon = (pokemon) => {
        addToTeam(pokemon);
    }


    const pokemonInfo = pokemon ? <Fragment>
        <div className='col-6  text-center'>

            <PokeImage sprites={pokemon.sprites} name={pokemon.name} types={pokemon.types} />
        </div>
        <div className='col-6'><p>Nº: {pokemon.id} </p>
            <p>Exp. base: {pokemon.base_experience}</p>
            <p>Weight: {pokemon.weight / 10} kg.</p>
            <p>Height: {pokemon.height * 10} cm.</p>
            <button className='btn btn-primary' onClick={() => catchPokemon(pokemon)}>
                {` Catch! `}
                <img alt='pokeBall'
                    className='image-custom'
                    src='https://upload.wikimedia.org/wikipedia/commons/thumb/5/53/Pok%C3%A9_Ball_icon.svg/1026px-Pok%C3%A9_Ball_icon.svg.png' />
            </button>
        </div>

    </Fragment> : <InitialCard message='Select a Pokemon!' />;

    return (<Card loading={loading} error={error}>
        {pokemonInfo}
    </Card>)
}
const mapStateToProps = ({ pokemonReducer, teamReducer }) => {
    return { ...pokemonReducer, ...teamReducer };
}

export default connect(mapStateToProps, teamActions)(PokemonMainInfo);
