import React from 'react';
import { connect } from 'react-redux';
import '../styles/pokeList.css';
import Layout from './shared/Layout';
import PokemonListItem from './PokemonListItem';

const PokemonList = ({ filtered_list, loading, error }) => {

    const listItems = filtered_list.map((d, i) => <PokemonListItem key={i} pokemon={d} index={i} />);

    return (
        <div className="panel panel-primary pb-2 custom-border" id="result_panel">
            <div className="panel-body">
                <div className="list-group ">
                    <Layout loading={loading} error={error}>
                        {filtered_list && listItems}
                    </Layout>
                </div>
            </div>
        </div>
    )
}

const mapStateToProps = ({ pokemonListReducer }) => {
    return pokemonListReducer;
}

export default connect(mapStateToProps)(PokemonList);
