export const GET_ALL_LIST = 'GET_ALL_LIST';
export const ERROR_LIST = 'ERROR_LIST';
export const LOADING_LIST = 'LOADING_LIST';
export const INDEX_SELECTED = 'INDEX_SELECTED';
export const FILTER_LIST = 'FILTER_LIST';
