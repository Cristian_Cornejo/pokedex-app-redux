import axios from "axios"
import { ERROR_LIST, FILTER_LIST, GET_ALL_LIST, LOADING_LIST } from "../types/pokemonListTypes";

export const getAllList = () => async (dispatch) => {
    dispatch({
        type: LOADING_LIST
    })

    try {
        const { data } = await axios.get('https://pokeapi.co/api/v2/pokemon?limit=151');
        dispatch({
            type: GET_ALL_LIST,
            payload: data.results
        })
    } catch (error) {
        dispatch({
            type: ERROR_LIST,
            payload: error.message
        })
    }
}

export const filterList = (name) => (dispatch) => {
    dispatch({
        type: FILTER_LIST,
        payload: name
    })
}