import { ADD_TO_TEAM, REMOVE_FROM_TEAM } from "../types/teamTypes";

export const addToTeam = (pokemon) => (dispatch, getState) => {
    const { team } = getState().teamReducer;
    if (team.length !== 6) {
        dispatch({
            type: ADD_TO_TEAM,
            payload: pokemon
        })
    }

}

export const removeFromTeam = (index) => (dispatch) => {
    dispatch({
        type: REMOVE_FROM_TEAM,
        payload: index
    })
}

