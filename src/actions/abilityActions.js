import axios from "axios";
import { ERROR_ABILITY, GET_ABILITY, LOADING_ABILITY } from "../types/abilityTypes";

export const getPokemon = (url) => async (dispatch) => {
    dispatch({
        type: LOADING_ABILITY
    })

    try {
        const { data } = await axios.get(url);
        const effect = data.effect_entries.filter((e =>
            e.language.name === "en"
        ));
        dispatch({
            type: GET_ABILITY,
            payload: effect[0].effect
        })
    } catch (error) {
        dispatch({
            type: ERROR_ABILITY,
            payload: error.message
        })
    }
}