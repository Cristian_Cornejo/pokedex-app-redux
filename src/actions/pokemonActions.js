import axios from "axios";
import { INDEX_SELECTED } from "../types/pokemonListTypes";
import { ERROR_POKEMON, GET_POKEMON, LOADING_POKEMON } from "../types/pokemonTypes";

export const getPokemon = (name, index) => async (dispatch) => {
    dispatch({
        type: INDEX_SELECTED,
        payload: index
    })
    dispatch({
        type: LOADING_POKEMON
    })

    try {
        const { data } = await axios.get(`https://pokeapi.co/api/v2/pokemon/${name}`);
        dispatch({
            type: GET_POKEMON,
            payload: data
        })
    } catch (error) {
        dispatch({
            type: ERROR_POKEMON,
            payload: error.message
        })
    }
}